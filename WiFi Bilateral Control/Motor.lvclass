﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"635F.31QU+!!.-6E.$4%*76Q!!%5!!!!13!!!!)!!!%3!!!!!3!!!!!1V.&lt;X2P=CZM&gt;G.M98.T!!!!!!#)%1#!#!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!0GHD%TWMA2-IV-&amp;F]ZFVA9!!!!-!!!!%!!!!!#8=^K5#D2*1[J&amp;2Z^YBFD?V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!I%G?S;&amp;`)%ON(WP(8$+EKA!!!"#9YOMY)&amp;,&amp;,KP`=&lt;&lt;].H&gt;,!!!!%"LD\0"2O`I\FIQ2:T@`=1Y!!!!1-&amp;]M+_':.$$AA)'"Z&lt;Z(0A!!!%-!!5R71U-847^U&lt;X)O&lt;(:D&lt;'&amp;T=TJ.&lt;X2P=CZD&gt;'Q!!!!!!!%!!F:*4%)!!!!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)1!!!"BYH'0A9G!39""A&amp;'#Q9""A-G!!MDG!G!%!#]="!!!!!!!!!%5!!!%E?*RD9-!%`Y%!3$%S-$$,!'F7.(%Q$7.4'S#&lt;C]U/80:C&amp;W=%CD(^!$*9A*A:KE9#)M@U!#D%AK[$$9P:!+KU*QI!!!!!!!!G!!&amp;73524&amp;UVP&gt;'^S,GRW9WRB=X-[47^U&lt;X)O9X2M!!!!!!!!!!-!!!!!!2Q!!!(Q?*Q,9'2AS$3W-*M!J*E:)#!Z0S562.]"CDF$R1Q0(!&lt;4-0ZBG&amp;IIX@S'R@"3]R'7FY````]0Z$^'C(?\K(!U6-AT],?+!17/.\AQ1K1[762?A*5=94FM?!"%AP1R133"GHC[@629/BMTG-%]A=\'(""L)ID`C6_!+9V@DY&amp;`^A(_+9=5$P'XMA$V]']\&gt;,A:L*[D'[SY.Y#D=S+',A=/IL6V1L5R1L2.0=!`[S$`\%0]5YYI8/%YS,`NT![18`B&gt;$RZ7:E4R&lt;T@(=5?2"*"X(46GA#E($T$F%M#)[H-A*@4L0_/&lt;`QY+)&amp;Q0Z/MQ-$*5!J7J!7G1M?Q-E%"B:'"BC'&amp;-:MRC,)4;BB]Y_\OY)P."]1E!&lt;4*KXQ!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!*A!"2F")5"&gt;.&lt;X2P=CZM&gt;G.M98.T/EVP&gt;'^S,G.U&lt;!!!!!!!!!!$!!!!!!1T!!!.BHC=T:&gt;&gt;;"R6&amp;-@0H:WN-]H'X)V*EZ(%`8!WFOJK+.CGQ7L44!K25G,4C0J3N^F9!\%LW9XIARO219B1#A9C#!7&amp;EI@[53%0PO2"SJ+8?&lt;#_#'Y,GQ&lt;[V)*6'R/3TI\HXNH:W9^U'Y06\-0F-HP`ZX]_@NS&gt;"7DYG,9*?5C&lt;1/A+&lt;I[:5"=X#%!G+E(B1Y@*"J"GB:BQ7"KGVY1];4@"&amp;T=[J3ZN'O\A57P*_AROEQ`J$4S[CSI9K=[%RLD2[H^:T6(V=LM[\86#_K'$HC.ZY45VM#K&gt;UR0I"PI?NPKD*!^%#YGC(B[-H2X66@:5DEI+$SG&lt;1$7D95,.0952U@I+$UGSM%&amp;O/3%"1_["_@FZ6_3X2:U]D7[GW5#6DW2L;*IUIS/JZJ\BGDKO)6H5&amp;(SU1(['Z=Z%F&gt;,(.)/C&amp;(8P/S6P/&amp;Z;Q$T-&gt;=P,S[D$N;"\WY2G.8&gt;%5K1&lt;];SXY28^"S"!-K=E[YLV+&gt;0\D\-J],../)B)$[&amp;$O"]S9;^O#'_#[(B^$D.]$+)TBB&gt;Q$/1I(Y/M46.QZT$TA$FI9C&lt;=U4=_G5S.4A14&lt;Q6(RG0*:0$&gt;C&lt;(X9KH29$S7CF60[*"GS!&gt;9^=S-QQ%N)-)-^*&gt;W/Q&amp;T=X09!&amp;R&gt;[9MI&lt;66T2:W@6T.1\(D=\4BT&gt;4PX%H:/7TNZBH5PUC.Q9$U=W)N]P=67]KA,\T[%VU]L]SO$^`F`(^\^S&amp;3[!F\IB43=K!(C!6N5!G];6??BNY;G'T64:@#GU??]YV-,XI06]+9&gt;LS+]M\/T:4I=15]28J%1'V\^KH80OM=1PGH&gt;B;^AG#0MY9J"(%@=W!5#-_F$EX@1B08`E5,`3@/GV.&lt;D!,JL$S!IA%WO^/LI?'*E,06"R3374.)3.U2ZHZKL+[/U%4LBE&amp;/KZ??FLK_P9[GYWN(V%(QE#LL+HMB2026Z]N3:1FC@389DQ0P6H-#D&gt;P'5+4KXAQ]D,3QM9#2=`1'M5[!+S8O#G9$&gt;9R]?#@)D)&lt;&lt;3='&amp;/)V+9B;]_SE919GPZ54HA:N/+W7#.HJ*MHE$^XOVH=XLLW:QOS[&lt;?*'W;I3!ED@9Y@X1\XI,:`*K^DNF-@P)FT]&lt;,,&amp;C5N3\&lt;DO_@+^E`7\+0OPP6HUM]::-I62&gt;+0:&lt;@_]!,B4S/.%&gt;?B]A&lt;I+W&gt;K,?P&amp;=^C5,,OW$_*CS&amp;JSNG'CT_5"@UA8DC-\30-5M(H@26MQ_`X:@PJ,&lt;,N0:K9'+G]9J:-_//BA!V`\C3QY?Z/!BN7`HOQY;^NAAWLGY!N`A/Q,TK8NP5&lt;0F=KQ&lt;ZU8\!0&lt;P83(EQERV*DC&lt;06&lt;(`^=.D_:E?R`?W/9PO\`Y(NS^NF_`N.W0:OA7XZ?-GLF(2N=&gt;,^&lt;ED6L&amp;I(;$^;4ZH1IP5V`51(M!U_;5$KJR@Q.20@/X&gt;,&amp;[36T#`&amp;PU?:9U7F@J6=]D3J8Q3ENL]"&lt;W`*[!!!!!!%!!!!0A!!!#9!!5*%3&amp;!847^U&lt;X)O&lt;(:D&lt;'&amp;T=TJ.&lt;X2P=CZD&gt;'Q!!!!!!!!!!Q!!!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!G9!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B%!A!A!!!!"!!A!-0````]!!1!!!!!!$A!!!!%!"A"1!!!!!1!!!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B%!A!A!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$/KYX`!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!TKO.`Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-2!)!)!!!!!1!)!$$`````!!%!!!!!!%M!!!!%!!^!#A!)5'^T;82J&lt;WY!!!N!#A!&amp;2G^S9W5!$U!+!!B7:7RP9WFU?1!!'E"1!!-!!!!"!!).47^U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!-!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:2%!A!A!!!!"!!5!!Q!!!1!!!!!!'!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%2!)!)!!!!"!!01!I!#&amp;"P=WFU;7^O!!!,1!I!"5:P=G.F!!^!#A!)6G6M&lt;W.J&gt;(E!!"J!5!!$!!!!!1!#$5VP&gt;'^S,GRW9WRB=X-!!1!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!&amp;!!Q!!!!%!!!"'A!!!#A!!!!#!!!%!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$A!!!&lt;6YH)V03UL%1"3MJ$MT_=QYY`]$1K^&gt;T-9,")4:+='&amp;;U-HE5"D3^)T[-Y,?",PYFU]A69_YM+.&amp;.VUV3N?61-YQS+.%7;WL6VN(Y'%.&amp;D&lt;2J@$Z+YU6N@O"&lt;D"Z]@\WR=!@XZNH7V7:KN.XL&lt;2Q,1T7+5:""U?`0-LMWF&gt;W3B&lt;K&gt;[GHJJ[G\N3&amp;&lt;H,%2#)58#&lt;B#12)]5F*ABRT^?-3A9@5`)9GILEZIN`:U4J&amp;+)S$QD46W&amp;PX3#5NM+SXT($($NC5V3-DT'+SQ\#02&gt;D_!+\(0PA.?EO$W-\C45.#PMYA/RR3-=0Z)C`SO`EK-@Q;Q`(V&amp;C#&amp;1*%6"0MY;4LR8.+FL!%Q\]"@=E_(Q!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!%5!!!!13!!!!)!!!%3!!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R01F.(!!!!!!!!!:"$1V.(!!!!!!!!!;2-38:J!!!!!!!!!&lt;B$4UZ1!!!!!!!!!=R544AQ!!!!!!!!!?"%2E24!!!!!!!!!@2-372T!!!!!!!!!AB735.%!!!!!!!!!BRW:8*T!!!!"!!!!D"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQY!!!!!!!!!LR$5%-S!!!!!!!!!N"-37:Q!!!!!!!!!O2'5%BC!!!!!!!!!PB'5&amp;.&amp;!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"A!!!!!!!!!!0````]!!!!!!!!!J!!!!!!!!!!!`````Q!!!!!!!!#Y!!!!!!!!!!$`````!!!!!!!!!-Q!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!%I!!!!!!!!!!$`````!!!!!!!!!4!!!!!!!!!!!0````]!!!!!!!!"7!!!!!!!!!!!`````Q!!!!!!!!'E!!!!!!!!!!$`````!!!!!!!!!&gt;!!!!!!!!!!"0````]!!!!!!!!#]!!!!!!!!!!(`````Q!!!!!!!!-%!!!!!!!!!!D`````!!!!!!!!!R1!!!!!!!!!#@````]!!!!!!!!$+!!!!!!!!!!+`````Q!!!!!!!!-Y!!!!!!!!!!$`````!!!!!!!!!UQ!!!!!!!!!!0````]!!!!!!!!$9!!!!!!!!!!!`````Q!!!!!!!!0E!!!!!!!!!!$`````!!!!!!!!"_A!!!!!!!!!!0````]!!!!!!!!(]!!!!!!!!!!!`````Q!!!!!!!!A=!!!!!!!!!!$`````!!!!!!!!$&amp;1!!!!!!!!!!0````]!!!!!!!!-8!!!!!!!!!!!`````Q!!!!!!!!S)!!!!!!!!!!$`````!!!!!!!!$0!!!!!!!!!!!0````]!!!!!!!!-_!!!!!!!!!!!`````Q!!!!!!!!^E!!!!!!!!!!$`````!!!!!!!!$WQ!!!!!!!!!!0````]!!!!!!!!0&gt;!!!!!!!!!!!`````Q!!!!!!!!_A!!!!!!!!!)$`````!!!!!!!!%,1!!!!!#5VP&gt;'^S,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!1V.&lt;X2P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!#!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!1!$U!+!!B1&lt;X.J&gt;'FP&lt;A!!#U!+!!6'&lt;X*D:1!01!I!#&amp;:F&lt;'^D;82Z!!"/!0(/KYX`!!!!!AV.&lt;X2P=CZM&gt;G.M98.T#5VP&gt;'^S,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!%1#!#!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Item Name="Motor.ctl" Type="Class Private Data" URL="Motor.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="CurrentToI16.vi" Type="VI" URL="../CurrentToI16.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#%!!!!"!!%!!!!%5!#!!N$&gt;8*S:7ZU)%ER.A!41!I!$%.V=H*F&lt;H1A17VQ=Q!!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!)#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!1!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="DegreesToEncoder.vi" Type="VI" URL="../DegreesToEncoder.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"[!!!!"!!%!!!!$5!+!!&gt;&amp;&lt;G.P:'6S!!V!#A!(2'6H=G6F=Q"5!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!A)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!"!!-!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="EncoderToDegrees.vi" Type="VI" URL="../EncoderToDegrees.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"[!!!!"!!%!!!!$5!+!!&gt;%:7&gt;S:76T!!V!#A!(27ZD&lt;W2F=A"5!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!A)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!"!!-!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="ForceToCurrent.vi" Type="VI" URL="../ForceToCurrent.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Y!!!!"!!%!!!!$5!+!!&gt;$&gt;8*S:7ZU!!N!#A!&amp;2G^S9W5!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!)#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!1!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="Read Force.vi" Type="VI" URL="../Read Force.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!#A!&amp;2G^S9W5!)E"Q!"Y!!!].47^U&lt;X)O&lt;(:D&lt;'&amp;T=Q!*47^U&lt;X)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!)E"Q!"Y!!!].47^U&lt;X)O&lt;(:D&lt;'&amp;T=Q!)47^U&lt;X)A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Read Position.vi" Type="VI" URL="../Read Position.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)5'^T;82J&lt;WY!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#5VP&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#%VP&gt;'^S)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Read Velocity.vi" Type="VI" URL="../Read Velocity.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)6G6M&lt;W.J&gt;(E!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#5VP&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#%VP&gt;'^S)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Write Force.vi" Type="VI" URL="../Write Force.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#5VP&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!#A!&amp;2G^S9W5!)E"Q!"Y!!!].47^U&lt;X)O&lt;(:D&lt;'&amp;T=Q!)47^U&lt;X)A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Write Position.vi" Type="VI" URL="../Write Position.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#5VP&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)5'^T;82J&lt;WY!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#%VP&gt;'^S)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Write Velocity.vi" Type="VI" URL="../Write Velocity.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#5VP&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)6G6M&lt;W.J&gt;(E!!#*!=!!?!!!0$5VP&gt;'^S,GRW9WRB=X-!#%VP&gt;'^S)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
</LVClass>
